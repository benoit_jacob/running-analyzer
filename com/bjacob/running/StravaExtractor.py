'''
Created on 15 nov. 2019

@author: b.jacob
'''

import time
import swagger_client
from swagger_client.rest import ApiException
import json
from datetime import date,datetime
import requests
from swagger_client.models.detailed_activity import DetailedActivity
import encodings
from pip._vendor.pytoml.writer import unicode
import json

#from pprint import pprint

class StravaExtractor(object):
    '''
    classdocs
    '''


    def __init__(self):
        self.token='04ed3614cbb00d6d297f2dd6d6b9d3691ed65586'
        self.elastic="http://localhost:9200/"
        '''
        Constructor
        '''  
    def cacheInElastic(self,id):
        stravaReq="https://www.strava.com/api/v3/activities/"+str(id)+"?include_all_efforts=%22%20%22"
        headers = {"Authorization": "Bearer "+self.token}

        response = requests.get(stravaReq,headers=headers)
        
        print(response.text)
        jsonData = response.text.encode("utf-8")
        if(self.elastic is not None):
            responseElastic =requests.put(self.elastic+"activity/_doc/"+str(id),data=jsonData,headers={'Content-Type': 'application/json'})
            print(responseElastic)
    
    def getAltitude(self,id):
        api_instance = swagger_client.StreamsApi()
        api_instance.api_client.configuration.access_token=self.token
        keys = ['altitude,distance']
        api_response = api_instance.get_activity_streams(id, keys, True)

        return api_response.altitude
    
    def getActivity(self,id):
        api_instance = swagger_client.ActivitiesApi()
        api_instance.api_client.configuration.access_token=self.token
        api_response = api_instance.get_activity_by_id(id)
        #with open('strava/activity_'+str(id)+'.json','w',encoding='utf-8') as outfile:
        #    outfile.write(str(api_response));
        
        self.cacheInElastic(id)
        
        print("activity {}",id)
        print(api_response)
        return api_response
    
    def getActivityLatLng(self,id):
        #https://www.strava.com/api/v3/activities/819436873/streams?keys=latlng
        api_instance = swagger_client.StreamsApi()
        api_instance.api_client.configuration.access_token=self.token
        keys = ['latlng']
        api_response = api_instance.get_activity_streams(id, keys, True)
        if(self.elastic is not None):
            jsonData='{"doc":{"latlng":'+json.dumps(api_response.latlng.data)+'}}'
            jsonData = jsonData.encode("utf-8")
            responseElastic =requests.post(self.elastic+"activity/_update/"+str(id),data=jsonData,headers={'Content-Type': 'application/json'})
            print(responseElastic)
        print("latlng : ")
       
        return api_response.latlng
        
    def getAthleteActivities(self,deb,fin):
       
        
        #https://www.strava.com/oauth/authorize?client_id=15340&redirect_uri=http://localhost&response_type=code&scope=activity:read
        #code=9dfc891c5089b51418c4eb1158c77d62fb9a13a0
        
        #https://www.strava.com/oauth/token?client_id=15340&client_secret=ed4d0ca56190ef2f4c8a75eeda2fc716ac2c53d1&code=48fae553c0ed7c8bb4319ae2b179fabaa3784219&grant_type=authorization_code
        #access_token=53813364843d4198b1f2aad60185aee035e6c35d
    
# Configure OAuth2 access token for authorization: strava_oauth
        swagger_client.configuration.access_token = self.token
        
# create an instance of the API class
        api_instance = swagger_client.ActivitiesApi()
        api_instance.api_client.configuration.access_token=self.token
        
        athlete_api = swagger_client.AthletesApi();
        athlete_api.api_client.configuration.access_token=self.token
        if fin is None:
            before = datetime.now().utcnow().timestamp()
        else:    
            before =  fin.timestamp()# Integer | An epoch timestamp to use for filtering activities that have taken place before a certain time. (optional)
        after = deb.timestamp() # 1572618293 Integer | An epoch timestamp to use for filtering activities that have taken place after a certain time. (optional)
        print("before :",before)
        print("after :",after)
        page = 1 # Integer | Page number. (optional)
        perPage = 100 # Integer | Number of items per page. Defaults to 30. (optional) (default to 30)
        ret = []
        print("getAthleteActivities")
        try: 
    # List Athlete Activities
            response = athlete_api.get_logged_in_athlete()
            print("ok get athlete")
            #print(response)
            api_response = api_instance.get_logged_in_athlete_activities(before=before, after=after, page=page, per_page=perPage)
            print(api_response)
            #activities_json = json.loads(api_response)
            for activity in api_response:
                print("activity id : "+str(activity._id))
                #print(activity)
                ret.append(activity._id)
        except ApiException as e:
            print("Exception when calling ActivitiesApi->getLoggedInAthleteActivities: %s\n" % e)
        return ret;  