'''
Created on 10 déc. 2019

@author: b.jacob
'''

class Category(object):
    '''
    classdocs
    '''

    DISTANCE_REF = 8
    RECOVERY_REF = 90

    def __init__(self, categoryName):
        '''
        Constructor
        '''
        self.categoryName = categoryName
        
    def calculateSpeedWeighted(self,activities,absolute:bool):
        activities = sorted(activities,key=lambda activity: activity.distanceLaps,reverse=True)
        if len(activities)>0:
            activityRef = activities[0]
            distanceRef = activityRef.distanceLaps
           
            print("categrory : "+self.categoryName+" --- distanceRef : "+str(distanceRef)+"--activityRef : "+str(activityRef))
            recoveryRef = activityRef.recoveryTime
            if absolute:
                distanceRef = Category.DISTANCE_REF
                recoveryRef = Category.RECOVERY_REF
            for activity in activities:
                deltaDistanceRatio = 0;
                deltaRecupRatio = 0;
                if distanceRef>0:
                    deltaDistanceRatio = (distanceRef - activity.distanceLaps)/distanceRef
                if recoveryRef>0:
                    deltaRecupRatio = (activity.recoveryTime - recoveryRef)/recoveryRef
                #10% -> 0,1
                activity.course = self.categoryName == "course"
                activity.setSpeedWeighted(deltaDistanceRatio,deltaRecupRatio)
