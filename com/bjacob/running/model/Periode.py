'''
Created on 14 déc. 2019

@author: b.jacob
'''
from com.bjacob.running.model.Week import Week


class Periode(object):
    '''
    classdocs
    '''
    NB_WEEKS=8

    def __init__(self, date,speedLapsWeighted):
        '''
        Constructor
        '''
        self.date = date
        self.speedLapsWeighted = speedLapsWeighted
        self.weeks = []
    
    def buildWeeks(self,activities,court,long):
        weekIndex = 0
        week = None
        for activity in activities:
            delta = self.date - activity.date
            
            if delta.days > 0 and len(self.weeks)<Periode.NB_WEEKS:
                deltaWeeks = delta.days//7;
                if deltaWeeks!=weekIndex :
                    weekIndex = deltaWeeks
                    week = Week(activity.date)
                    self.weeks.append(week)
                if week is not None:
                    if activity.category in court:
                        week.addCourt(activity)
                    elif activity.category in long:
                        week.addLong(activity)
                
    def addValInX(self,X,p):
        index=0
        for w in self.weeks:
            w.addValInX(X,p,index)
            index = index + 6        
            
            
        
    def addActivities(self,week):
        self.weeks.append(week)
    
    def __str__(self):
        s = str(self.date) + "-->"+str(self.speedLapsWeighted)
        for w in self.weeks:
            s = s + "\n  w : "+str(w)
        return s;    