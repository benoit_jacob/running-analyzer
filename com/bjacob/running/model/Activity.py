'''
Created on 19 nov. 2019

@author: b.jacob
'''
from swagger_client.models.detailed_activity import DetailedActivity
from com.bjacob.running.model.Lap import Lap

class Activity(object):
    
    DELTA_SPEED = 2.0
    
    MIN_SPEED_LAP = 13
    
    NB_SERIES = 19
    
    SEUIL_DISTANCE_LONGUE = 15
    
    '''
    classdocs
    '''


    def __init__(self, detailedActivity: DetailedActivity,jsonActivity=None):
        '''
        Constructor
        '''
        self.init()
        if jsonActivity is not None:
           self.buildLapsJson(jsonActivity)
           if 'elev_high' in jsonActivity and 'elev_low' in jsonActivity:
               self.elevationGain = jsonActivity['elev_high'] - jsonActivity['elev_low']
           else :
               self.elevationGain = 0
        else:     
            self.buildLaps(detailedActivity)
        
    def init(self):
        self.laps = []
        self.name = ""
        self.category=""
        self.piste = False
        self.course = False
        self.inertia = 0
        self.cote = False
        self.speedLaps=0.0
        self.recoveryTime=0
        self.distanceLaps = 0;
        
        self.series = {100:0,200:0,300:0,400:0,500:0,600:0,800:0,1000:0,1500:0,2000:0,2500:0,3000:0,3500:0,4000:0,4500:0,5000:0}
                
    def buildLaps(self,detailedActivity: DetailedActivity):
        self.distance = detailedActivity.distance
        self.date = detailedActivity.start_date
        prec = False
        for l in detailedActivity.laps:
            delta = l.average_speed*3.6-detailedActivity.average_speed*3.6
            
            if delta>Activity.DELTA_SPEED or (l.average_speed*3.6)>= Activity.MIN_SPEED_LAP:
                lap = Lap(l)
                if prec == False:
                    self.laps.append(lap)
                else:
                    self.laps[-1].merge(l);
                prec = True
            else:
                prec = False
        self.generateName()
        self.calculateSpeedLaps()
        
    def buildLapsJson(self,jsonActivity):
        self.distance = jsonActivity['distance']
        self.date = jsonActivity['start_date']
        self.time = jsonActivity['moving_time']
        self.id=jsonActivity['id']
        if 'category' in jsonActivity:
            self.category=jsonActivity['category']
        prec = False
        if 'laps' in jsonActivity.keys():
            recovery = 0;
            for l in jsonActivity['laps']:
                delta = l['average_speed']*3.6-jsonActivity['average_speed']*3.6
                
                if delta>Activity.DELTA_SPEED or (l['average_speed']*3.6)>= Activity.MIN_SPEED_LAP:
                    lap = Lap(lap=None, lapJson=l)
                    if prec == False:
                        self.laps.append(lap)
                        self.recoveryTime = self.recoveryTime + recovery;
                        #print("recoveryTime : "+str(self.recoveryTime))
                        recovery = 0;
                    else:
                        self.laps[-1].merge(lap=None,lapJson=l);
                    prec = True
                else:
                    if len(self.laps)>0:
                        recovery = recovery + l['elapsed_time']
                    prec = False
        
        if len(self.laps)>1:
            #print("recoveryTime : "+str(self.recoveryTime))
            self.recoveryTime = self.recoveryTime/(len(self.laps)-1)
            self.recoveryTime = round(self.recoveryTime, 2)
            print("temps moyen recup : "+str(self.recoveryTime))            
        self.generateName()
        self.calculateSpeedLaps()
                
    def generateName(self):
        self.laps = sorted(self.laps, key=lambda lap: lap.distance)
        d = 0;
        nb = 0;
        nbCotes = 0;
        distanceLaps = 0;
        for l in self.laps:
            if l.cote:
                nbCotes=nbCotes+1
            if l.distance != d:
                if nb > 0 and d >0:
                    self.name = self.name + str(nb)+"*"+str(d)+"_"
                    if d in self.series:
                        self.series[d]=nb
                d = l.distance
                nb = 1
            else:
                nb=nb +1
            distanceLaps= distanceLaps + l.distance
        self.name = self.name + str(nb)+"*"+str(d)
        if nbCotes >= (len(self.laps)-2) and nbCotes>4:
            self.name = self.name + "_cote"
            self.cote = True
        
        if abs(distanceLaps - self.distance)/self.distance<0.1:
            self.name = self.name + "_course"
            self.course = True
        if nb == 0:
            self.name = "footing"
        else:
            if d in self.series:
                self.series[d]=nb
    
    def setPiste(self,piste):
        if not self.name.endswith("cote"):
            self.piste = piste
            for l in self.laps:
                l.calculSpeed(piste)
                
    def __str__(self):
        return self.name + "-"+str(self.date)+"("+str(self.id)+")"
    
    def getMainDistanceSerie(self):
        mainDistance = 100;
        nb = 0;
        for d  in self.series:
            if self.series[d]>nb:
                mainDistance = d
                nb = self.series[d]
        return mainDistance
    
    def checkLaps(self):
        #verif coherence des tours pour supprimer les tours 'foireux'
        lapsToRemove = []
        if self.speedLaps>0:
            for lap in self.laps:
                speedLap = (lap.originalDistance/lap.time)*3.6
                if self.piste:
                    speedLap = (lap.distance/lap.time)*3.6
                deltaSpeed = (self.speedLaps-speedLap)/self.speedLaps
                if deltaSpeed > 0.08:
                    #ecart trop important -> lap foireux
                    lapsToRemove.append(lap)
            
        for lap in lapsToRemove:
            self.laps.remove(lap)
            print("suppr lap : "+str(lap)+" pour : "+str(self))
        
    
    def calculateSpeedLaps(self):
        self.checkLaps()
        self.speedLaps=0.0
        self.distanceLaps = 0;
        totalTime=0
        totalDistance=0
        for lap in self.laps:
            totalTime = totalTime + lap.time;
            if self.piste:
                totalDistance = totalDistance + lap.distance
            else:    
                totalDistance = totalDistance + lap.originalDistance
        if totalTime >0:    
            self.speedLaps = (totalDistance/totalTime)*3.6
            self.speedLaps = round(self.speedLaps, 2)
        self.distanceLaps = totalDistance/1000;
       
        print("speedLaps : "+str(self.speedLaps)+" pour : "+str(self)+" nb laps : "+str(len(self.laps)))
        
    def setSpeedWeighted(self,deltaDistanceRatio,deltaRecupRatio):
        if self.speedLaps>0 :
            deltaRecupRatio = max(deltaRecupRatio,-0.5)
            deltaRecupRatio = min(deltaRecupRatio,0.5)
            if len(self.laps)==1:
                deltaRecupRatio = 0
            self.speedWeighted = self.speedLaps - deltaDistanceRatio - deltaRecupRatio;
            self.speedWeighted = round(self.speedWeighted,2)
            
            if self.distance/1000 > Activity.SEUIL_DISTANCE_LONGUE and not self.course:
                #bonus sortie longue avec allure specifique
                self.speedWeighted = self.speedWeighted + ((self.distance/1000-Activity.SEUIL_DISTANCE_LONGUE)*0.1)
                                                           
            
            if self.course:
                #prise en compte du denivellé que pour les courses. bareme : 90m -> 2% vitesse
                loss = self.elevationGain * 0.02/90
                self.speedWeighted = (self.distance/self.time)*3.6
                self.speedWeighted = self.speedWeighted + self.speedWeighted*loss
                print("course : "+str(self)+" --> "+str(self.speedWeighted)+" -- loss : "+str(loss))
                if loss > 0.05:
                    print('!! elevationGain fort pour '+str(self))
            
            print(str(self)+" -- speedLaps : "+str(self.speedLaps))
            print("speedWeighted : "+str(self.speedWeighted)+" -- deltaDistanceRatio : "+str(deltaDistanceRatio)+"---deltaRecupRatio: "+str(deltaRecupRatio))
        else:
            self.speedWeighted = self.speedLaps;         