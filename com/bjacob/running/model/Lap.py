'''
Created on 19 nov. 2019

@author: b.jacob
'''
from swagger_client.models import Lap
import json

class Lap(object):
    '''
    classdocs
    '''


    def __init__(self, lap: Lap, lapJson):
        '''
        Constructor
        '''
        if lapJson is not None:
            self.time=lapJson['elapsed_time']
            deltaTime =  lapJson['elapsed_time'] - lapJson['moving_time']
            if deltaTime > 10:
                #cas d'un arrêt pendant le tour -> prendre moving _time
                self.time = lapJson['moving_time']
            
            self.originalDistance = lapJson['distance']
            self.distance=round(round(lapJson['distance'])/100)*100
            self.cote = False
            if self.distance >0:
                elevation=lapJson['total_elevation_gain']/self.distance
                if elevation > 0.04:
                    self.cote = True
            
            if self.distance >= 2000:
                self.distance=(lapJson['distance']//1000)*1000
        else:
            self.time=lap.elapsed_time
            self.originalDistance = lap.distance
            self.distance=round(round(lap.distance)/100)*100
            if self.distance >= 2000:
                self.distance=(lap.distance//1000)*1000
        self.speed=0.0
        
    def merge(self, lap,lapJson):
        if lapJson is not None:
            deltaTime =  lapJson['elapsed_time'] - lapJson['moving_time']
            
            if deltaTime > 10:
                #cas d'un arrêt pendant le tour -> prendre moving _time
                self.time = self.time + lapJson['moving_time']
            else :
                self.time=self.time + lapJson['elapsed_time']
            self.distance = self.distance + round(round(lapJson['distance'])/100)*100
            self.originalDistance = self.originalDistance + lapJson['distance']
            if self.distance >= 1000:
                self.distance=((self.distance)//500)*500
        else:
            self.time=self.time + lap.elapsed_time
            self.distance = self.distance + round(round(lap.distance)/100)*100
            self.originalDistance = self.originalDistance + lap.distance
            if self.distance >= 1000:
                self.distance=((self.distance)//500)*500
                
    def checkDistance(self):
        if self.originalDistance > 500:
            delta = abs(self.distance - self.originalDistance)/self.distance
            oldDistance = self.distance
            if delta > 0.2:
                 self.distance=round(round(self.originalDistance)//100)*100
                 print("correction distance lap : "+str(oldDistance)+" -->"+str(self.distance)+"--originalD : "+str(self.originalDistance))
    
    def calculSpeed(self,piste):
        self.checkDistance()
        if piste:
            self.speed = (self.distance/self.time)*3,6
        else:
            self.speed = (self.originalDistance/self.time)*3,6
        if len(self.speed)>0:
                self.speed = self.speed[0]
    
    def __str__(self):
        return str(self.distance) + "-"+str(self.time)
    
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)    