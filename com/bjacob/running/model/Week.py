'''
Created on 14 déc. 2019

@author: b.jacob
'''

class Week(object):
    '''
    classdocs
    '''


    def __init__(self, date):
        '''
        Constructor
        '''
        self.date = date
        self.activities = {}
        self.activities['court']=None
        self.activities['long']=None
        
    def addCourt(self,activity):
        self.activities['court']=activity
    
        
    def addLong(self,activity):
        self.activities['long']=activity
        
    def addValInX(self,X,p,index):
        if self.activities['court'] is not None:
            X[p][index] = self.activities['court'].distanceLaps
            X[p][index+1]= self.activities['court'].recoveryTime
            X[p][index+2]= self.activities['court'].speedLaps
            if self.activities['court'].distanceLaps < 0:
                print("!! distance negative pour "+str(self.activities['court']))
            if self.activities['court'].recoveryTime < 0:
                print("!! recoveryTime negative pour "+str(self.activities['court']))
            if self.activities['court'].speedLaps < 0:
                print("!! speedLaps negative pour "+str(self.activities['court']))
        else :
            X[p][index] = 0
            X[p][index+1]= 0
            X[p][index+2]= 0
        if self.activities['long'] is not None:
            X[p][index+3] = self.activities['long'].distanceLaps
            X[p][index+4]= self.activities['long'].recoveryTime
            X[p][index+5]= self.activities['long'].speedLaps
            if self.activities['long'].distanceLaps < 0:
                print("!! distance negative pour "+str(self.activities['long']))
            if self.activities['long'].recoveryTime < 0:
                print("!! recoveryTime negative pour "+str(self.activities['long']))
            if self.activities['long'].speedLaps < 0:
                print("!! speedLaps negative pour "+str(self.activities['long']))
        else :
            X[p][index+3] = 0
            X[p][index+4]= 0
            X[p][index+5]= 0
        
        
    def __str__(self):
        return str(self.date) + "\n -court : "+str(self.activities['court'])+"\n -long :"+str(self.activities['long'])