'''
Created on 27 nov. 2019

@author: b.jacob
'''
from com.bjacob.running.learn.ML import ML
from com.bjacob.running.ElasticRepository import ElasticRepository
from com.bjacob.running.model.Activity import Activity

if __name__ == '__main__':
    pass

deb = '2013-10-22'
fin = '2013-10-23'
elasticRepo = ElasticRepository()
activity_ids = elasticRepo.getAthleteActivities(deb, fin)
print("nombre d'activités : ",len(activity_ids))
print("activities ------------------------------------------------------")
ml = ML(None)
activities = [];
for id in activity_ids:
    activityJson = elasticRepo.getActivity(id['_id'])
    activity = Activity(detailedActivity=None,jsonActivity=activityJson)
    if 'latlng' in activityJson.keys():
        latlng = activityJson['latlng']
        ml.setPiste(activity,latlng)
    print("activity du ",activity.date,activity.name)
    activities.append(activity)
    
#ml.classify(activities)    