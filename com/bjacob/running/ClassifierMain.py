'''
Created on 27 nov. 2019

@author: b.jacob
'''
from com.bjacob.running.learn.ML import ML
from com.bjacob.running.ElasticRepository import ElasticRepository
from com.bjacob.running.model.Activity import Activity
from com.bjacob.running.learn.Classifier import Classifier

if __name__ == '__main__':
    pass

elasticRepo = ElasticRepository()
activity_classees_ids = elasticRepo.getActivitiesNotcX()
print("nombre d'activités classes: ",len(activity_classees_ids))
activity_non_classees_ids = elasticRepo.getActivitiescX()
print("nombre d'activités non classes: ",len(activity_non_classees_ids))
print("activities ------------------------------------------------------")
ml = ML(None)
activitiesClasses = [];
activitiesNonClasses = [];
for id in activity_classees_ids:
    activityJson = elasticRepo.getActivity(id['_id'])
    if 'distance' in activityJson:
        activity = Activity(detailedActivity=None,jsonActivity=activityJson)
        if 'latlng' in activityJson.keys():
            latlng = activityJson['latlng']
            ml.setPiste(activity,latlng)
        print("activity du ",activity.date,activity.name)
        activitiesClasses.append(activity)
    
for id in activity_non_classees_ids:
    activityJson = elasticRepo.getActivity(id['_id'])
    activity = Activity(detailedActivity=None,jsonActivity=activityJson)
    if 'latlng' in activityJson.keys():
        latlng = activityJson['latlng']
        ml.setPiste(activity,latlng)
    print("activity du ",activity.date,activity.name)
    activitiesNonClasses.append(activity)
    
classifier = Classifier()
classifier.classify(activitiesClasses, activitiesNonClasses)   