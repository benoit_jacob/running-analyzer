'''
Created on 20 nov. 2019

@author: b.jacob
'''

from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans

def test():
        print("test kmeans")
        X, y = make_blobs(
            n_samples=52, n_features=16,
            centers=3, cluster_std=0.5,
            shuffle=True, random_state=0
            )
        print(X)
        km = KMeans(n_clusters=10, init='random',n_init=10, max_iter=300, tol=1e-04, random_state=0)
        y_km = km.fit(X)
        print(y_km)
        print("fin test kmeans")

if __name__ == '__main__':
    test()