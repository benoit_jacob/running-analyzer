'''
Created on 20 nov. 2019

@author: b.jacob
'''
from sklearn.datasets import make_blobs
from sklearn.linear_model import SGDClassifier
from com.bjacob.running.ElasticRepository import ElasticRepository
from com.bjacob.running.model.Activity import Activity

class Classifier(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        

    def buildMatrix(self, X, i, activity):
        j = 0
        for k in activity.series.keys():
            #Y.append(str(i)+"_"+str(k))
            #Y.append(v)
            val = activity.series[k]
            X[i][j] = val
            j = j + 1
        
        piste = 0
        course = 0
        cote = 0
        if activity.piste:
            piste = 10
        if activity.cote:
            cote = 15
        if activity.course:
            course = 10
            cote = 0
            piste = 0
        X[i][j] = piste
        j = j + 1
        X[i][j] = course
        j = j + 1
        X[i][j] = cote

    def countCategories(self,activitiesClassees):
        distinctCategorie = {};
        for activity in activitiesClassees:
            distinctCategorie[activity.category]=activity.category
        
        return len(distinctCategorie.keys());    

    def classify(self,activitiesClassees,activitiesNonClassees):
        nbCategories = self.countCategories(activitiesClassees)
        print("nbCategories : "+str(nbCategories))
        X, y = make_blobs(n_samples=len(activitiesClassees), n_features=19,centers=3, cluster_std=0.5,shuffle=True, random_state=0)
        y=[]
        i=0;
        for activity in activitiesClassees:
            y.append(activity.category)
            self.buildMatrix(X, i, activity)
            i=i+1
        
        X1, y1 = make_blobs(n_samples=len(activitiesNonClassees), n_features=Activity.NB_SERIES,centers=3, cluster_std=0.5,shuffle=True, random_state=0)
        i=0;    
        for activity in activitiesNonClassees:
            self.buildMatrix(X1, i, activity)
            i=i+1
        
        sgdc = SGDClassifier(random_state=42)
        sgdc.fit(X,y)
        y1=sgdc.predict(X1)
        
        i=0;
        elastic = ElasticRepository()
        for activity in activitiesNonClassees:
            activity.category = y1[i]
            print(activity)
            print("-->"+str(activity.category))
            elastic.updateActivityCategorie(activity)
            i=i+1    