'''
Created on 20 nov. 2019

@author: b.jacob
'''

from sklearn.datasets import make_regression
from sklearn import linear_model
from sklearn import svm
from sklearn import tree
from sklearn.ensemble import GradientBoostingRegressor
from xgboost import XGBRegressor

def createXy():
    X, y = make_regression(
            n_samples=9, n_features=3,
            shuffle=True, random_state=0
            )
    X[0][0]=3
    X[0][1]=50
    X[0][2]=17.25
    y[0]=15.25
    
    X[1][0]=3.5
    X[1][1]=52
    X[1][2]=17.05
    y[1]=15.35
    
    X[2][0]=2.85
    X[2][1]=45
    X[2][2]=17.75
    y[2]=15.05
    
    X[3][0]=3.8
    X[3][1]=58
    X[3][2]=17.0
    y[3]=15.0
    
    X[4][0]=4
    X[4][1]=60
    X[4][2]=16.85
    y[4]=15.30
    
    X[5][0]=4.5
    X[5][1]=65
    X[5][2]=16.5
    y[5]=15.5
    
    X[6][0]=2.65
    X[6][1]=40
    X[6][2]=18.0
    y[6]=15.7
    
    X[7][0]=3.2
    X[7][1]=55
    X[7][2]=17.75
    y[7]=15.4
    
    X[8][0]=5
    X[8][1]=70
    X[8][2]=16.25
    y[8]=15.0
    
    return X,y    

def createX1y1():
    X1, y1 = make_regression(
            n_samples=1, n_features=3,
            shuffle=True, random_state=0
            )
    X1[0][0]=2
    X1[0][1]=50
    X1[0][2]=15.25
    y1=15.25
    
    return X1,y1    

def testLinear():
    print("test linear")
    X, y =createXy();
    X1, y1 = createX1y1()
    print(X)
    print(y)
    regr = linear_model.LinearRegression()
    regr.fit(X, y)
        
    print(X1)
    print(regr.predict(X1)[0])
    print("fin test linear regression")

def testSVM():
    print("test SVM")
    X, y =createXy();
    X1, y1 = createX1y1()
    print(X)
    print(y)
    clf = svm.SVR(gamma='scale')
    clf.fit(X, y)
        
    print(X1)
    print(clf.predict(X1)[0])
    print("fin test SVM")

def testDecisionTree():
    X, y =createXy();
    X1, y1 = createX1y1()
    print(X)
    print(y)
    clf = tree.DecisionTreeRegressor()
    clf = clf.fit(X, y)
    print(X1)
    print(clf.predict(X1)[0])
    print("fin test DecisionTree")
    
def testGradientBoosting():
    X, y =createXy();
    X1, y1 = createX1y1()
    print(X)
    print(y)
    est = GradientBoostingRegressor(n_estimators=100, learning_rate=0.1, max_depth=1, random_state=0, loss='ls')
    est = est.fit(X, y)
    print(X1)
    print(est.predict(X1)[0])
    print("fin test GradientBoosting")
    
def testXGB():
    X, y =createXy();
    X1, y1 = createX1y1()
    print(X)
    print(y)
    est = XGBRegressor(objective="reg:squarederror")
    est = est.fit(X, y)
    print(X1)
    print(est.predict(X1)[0])
    print("fin test XGB")

if __name__ == '__main__':
    testLinear()
    print("-----------------------------")
    testSVM()
    print("-----------------------------")
    testDecisionTree()
    print("-----------------------------")
    testGradientBoosting()
    print("-----------------------------")
    testXGB()
    