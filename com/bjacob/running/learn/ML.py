'''
Created on 19 nov. 2019

@author: b.jacob
'''
from com.bjacob.running.model.Activity import Activity
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
import numpy as np
import matplotlib.pyplot as plt
from com.bjacob.running.ElasticRepository import ElasticRepository

class ML(object):
    '''
    classdocs
    '''

    #distance max avec centraoid.
    INERTIA_PISTE = 0.02
    
    #pour eviter d'avoir de trop grosses diff ex : 16*200 se retrouve dans un autre cluster que 10*200
    MAX_SERIES = 8

    def __init__(self, params):
        '''
        Constructor
    
        '''
    
    def setPiste(self, activity: Activity, latlng): 
       
        kmeans = KMeans(3)
       
        kmeans = kmeans.fit(latlng)
        #print("centers : ")
        #print(kmeans.cluster_centers_)
        
        #print("inertia_ : ")
        #print(kmeans.inertia_)
        piste = kmeans.inertia_<ML.INERTIA_PISTE and len(activity.laps)>0
        activity.setPiste(piste)
        activity.inertia=kmeans.inertia_
        if activity.piste:
            activity.name = activity.name + "_piste"
    
    def classify(self, activities):
        X, y = make_blobs(n_samples=len(activities), n_features=Activity.NB_SERIES,centers=3, cluster_std=0.5,shuffle=True, random_state=0)
        i=0
        for activity in activities:
            j=0
            print("activity.series.keys() : ",len(activity.series.keys()))
            for k in activity.series.keys():
                
                #Y.append(str(i)+"_"+str(k))
                #Y.append(v)
                val = activity.series[k]
                if val > ML.MAX_SERIES:
                    val = ML.MAX_SERIES
                X[i][j]= val
                j=j+1
            #X.append(Y)
            piste = 0
            course = 0
            cote = 0
            if activity.piste:
                piste = 10
            if activity.cote:
                cote = 15
            if activity.course:
                course = 10
                cote = 0
                piste = 0
                
            X[i][j]=piste
            j=j+1
            X[i][j]=course
            j=j+1
            X[i][j]=cote
            i=i+1
        nbCluster=20;
        if len(activities)<nbCluster:
            nbCluster=len(activities)//2
        kmeans = KMeans(n_clusters=nbCluster, init='random',n_init=10, max_iter=500, tol=1e-04, random_state=0)
        print(X)
        kmeans = kmeans.fit(X)
        print("centers : ")
        print(kmeans.cluster_centers_)
        
        distancesFromCenters = kmeans.transform(X);
        for d in distancesFromCenters:
            print("distance : "+str(min(d)))
        clusterDict = self.getClusters(kmeans,activities,distancesFromCenters)
        #self.plotClusters(X, activities, kmeans, clusterDict)
        
    def getClusters(self,kmeans: KMeans,activities,distancesFromCenters):
        ret = {}
        elastic = ElasticRepository();
        mapIndexDistance = {}
        if len(activities)>0:
            index = 0;
            for k in activities[0].series.keys():
                mapIndexDistance[index]=k
                index = index+1
            for indexActivity in range(0,len(activities)-1):
                clusterIndex = kmeans.labels_[indexActivity]
                listActivitiesCluster = None
                if clusterIndex in ret:
                    listActivitiesCluster = ret[clusterIndex]
                else:
                    listActivitiesCluster = []
                    ret[clusterIndex]=listActivitiesCluster
                listActivitiesCluster.append(activities[indexActivity])
        for k in ret.keys():
            print("Cluster ",k)
            #elastic.createCluster("c"+str(k), ret[k])
            for a in ret[k]:
                indexA = activities.index(a)
                distanceA = distancesFromCenters[indexA]
                minDistance = min(distanceA)
                print(a)
                print("--distance : "+str(minDistance))
                a.category = "c"+str(k)
                if minDistance>=3:
                    a.category="cX"
                elastic.updateActivity(a);
            print()
        
        return ret
    
    def plotClusters(self,X,activities,kmeans,clusterDict):
        colors = {0:'green', 1:'pink', 2:'yellow',3:'orange',4:'lightgreen',5:'lightblue',6:'blue',7:'red',8:'black',9:'grey',10:'purple',11:'cyan',12:'magenta',13:'navy',14:'royalblue',15:'plum',16:'violet',17:'lime',18:'tomato',19:'tan',20: 'aqua'}
        for k in clusterDict.keys():
            for activity in clusterDict[k]:
                nb = activity.name[0]
                distance = activity.getMainDistanceSerie()
                plt.scatter(distance, nb,s=50, c=colors[k],marker='s', edgecolor='black',label=activity.name, vmax=20)
                plt.text(distance, nb, activity.name)
        #plt.legend(scatterpoints=1)
        plt.grid()
        plt.show()    