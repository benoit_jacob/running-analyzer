'''
Created on 18 nov. 2019

@author: b.jacob
'''

from com.bjacob.running.StravaExtractor import StravaExtractor
from datetime import datetime,date
from swagger_client.models import detailed_activity
from com.bjacob.running.model.Activity import Activity
from com.bjacob.running.learn.ML import ML

if __name__ == '__main__':
    pass
#deb = datetime(2011,8,1);
#fin = datetime(2011,12,31);

deb = datetime(2016,1,30)
fin = datetime(2016,2,2)
extractor = StravaExtractor()
activity_ids = extractor.getAthleteActivities(deb, fin)
print("nombre d'activités : ",len(activity_ids))
print("activities ------------------------------------------------------")
ml = ML(None)
activities = [];
for id in activity_ids:
    detailed_activity = extractor.getActivity(id)
    activity = Activity(detailed_activity)
    
    latlng = extractor.getActivityLatLng(id)
    ml.setPiste(activity,latlng)
    print("activity du ",activity.date,activity.name)
    activities.append(activity)
    
ml.classify(activities)
    