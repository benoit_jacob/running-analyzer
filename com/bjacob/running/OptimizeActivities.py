'''
Created on 27 nov. 2019

@author: b.jacob
'''
from com.bjacob.running.learn.ML import ML
from com.bjacob.running.ElasticRepository import ElasticRepository
from com.bjacob.running.model.Activity import Activity
from com.bjacob.running.learn.Classifier import Classifier
from com.bjacob.running.model.Category import Category
from builtins import sorted
from com.bjacob.running.model.Periode import Periode
from datetime import datetime
from com.bjacob.running.learn.Regression import Regression

if __name__ == '__main__':
    pass

deb = '2010-01-01'
fin = '2019-12-27'
elasticRepo = ElasticRepository()
activity_ids = elasticRepo.getAthleteActivities(deb, fin)
print("nombre d'activités : ",len(activity_ids))
print("activities ------------------------------------------------------")
ml = ML(None)
activities = [];
regression = Regression()

fractionne = {}
fractionne['court']=['1min','2min','1min-2min','200m_piste','300m_piste','400m_piste','400m','500m_piste','pyramide','diagonales','30-30','cotes','800m_piste','200_400_piste','800m','pyramide_400_600','500-1000']
fractionne['long']=['fractionne_long_piste','fractionne_long','1000m_piste','1000m','course']

def parseActivities(activites):
    periodes = []
    activites= sorted(activites,key=lambda activity: activity.date,reverse=True)
    for activity in activites:
        if activity.category in fractionne['long']:
            periode = Periode(activity.date,activity.speedWeighted)
            if activity.speedWeighted<14:
                print("!! speedLapsWeighted faible : "+str(activity.speedWeighted)+" -- "+str(activity)+" -- speedLaps : "+str(activity.speedLaps))
            periode.buildWeeks(activites,fractionne['court'],fractionne['long'])
            if len(periode.weeks)==Periode.NB_WEEKS:
                periodes.append(periode)
            
            
    return periodes    

categoriesDict = {}
for id in activity_ids:
    activityJson = elasticRepo.getActivity(id['_id'])
    if 'distance' in activityJson:
        activity = Activity(detailedActivity=None,jsonActivity=activityJson)
        if 'latlng' in activityJson.keys():
            latlng = activityJson['latlng']
        ml.setPiste(activity,latlng)
        activity.calculateSpeedLaps()
        activities.append(activity)
        activity.date = datetime.fromisoformat(activity.date[0:10])
        activitiesCategory = []
        if activity.category in categoriesDict:
            activitiesCategory = categoriesDict[activity.category]
            
        else:
            activitiesCategory = []
            categoriesDict[activity.category]=activitiesCategory
        activitiesCategory.append(activity)
        
for c in categoriesDict.keys():
    category = Category(c)
    activitiesCategory = categoriesDict[c]
    category.calculateSpeedWeighted(activitiesCategory,True)
       
periodes = parseActivities(activities)


print("nb periodes : "+str(len(periodes)))

periodToPredict = []

index = 0;
for p in periodes:
    print(p);
    print('----------------------------------')
    if index%8 == 0:
        periodToPredict.append(periodes.pop(index))
    index = index + 1
        
print("periode to predict, nb : "+str(len(periodToPredict)))

print(periodToPredict)

regression.buildModel(periodes)

regression.predictList(periodToPredict)