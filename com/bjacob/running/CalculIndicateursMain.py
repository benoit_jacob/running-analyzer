'''
Created on 27 nov. 2019

@author: b.jacob
'''
from com.bjacob.running.learn.ML import ML
from com.bjacob.running.ElasticRepository import ElasticRepository
from com.bjacob.running.model.Activity import Activity
from com.bjacob.running.learn.Classifier import Classifier
from com.bjacob.running.model.Category import Category

if __name__ == '__main__':
    pass

deb = '2015-05-17'
fin = '2015-05-18'
elasticRepo = ElasticRepository()
activity_ids = elasticRepo.getAthleteActivities(deb, fin)
print("nombre d'activités : ",len(activity_ids))
print("activities ------------------------------------------------------")
ml = ML(None)
activitiesClasses = [];
activitiesNonClasses = [];
categoriesDict = {}
for id in activity_ids:
    activityJson = elasticRepo.getActivity(id['_id'])
    if 'distance' in activityJson:
        activity = Activity(detailedActivity=None,jsonActivity=activityJson)
        if 'latlng' in activityJson.keys():
            latlng = activityJson['latlng']
        ml.setPiste(activity,latlng)
        activity.calculateSpeedLaps()
        activitiesCategory = []
        if activity.category in categoriesDict:
            activitiesCategory = categoriesDict[activity.category]
            
        else:
            activitiesCategory = []
            categoriesDict[activity.category]=activitiesCategory
        activitiesCategory.append(activity)
        #elasticRepo.updateActivityIndicateurs(activity)
    else:
        print("pas de distance pour :"+str(activityJson['start_date']))
        
for c in categoriesDict.keys():
    category = Category(c)
    activitiesCategory = categoriesDict[c]
    category.calculateSpeedWeighted(activitiesCategory)
    for activity in activitiesCategory:
        elasticRepo.updateActivityIndicateurs(activity)
        elasticRepo.updateActivityLabel(activity)
