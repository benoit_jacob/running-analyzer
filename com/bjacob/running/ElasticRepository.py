'''
Created on 26 nov. 2019

@author: b.jacob
'''
import requests
import json
from datetime import datetime,date

from com.bjacob.running.model.Activity import Activity

class ElasticRepository(object):
    '''
    classdocs
    '''


    def __init__(self):
        self.elasticUrl="http://localhost:9200/"
        '''
        Constructor
        '''
    def getActivity(self,id):
        response = requests.get(self.elasticUrl+"activity/_doc/"+str(id))
        jsonData = json.loads(response.text)
        return jsonData['_source']
    
    def getAthleteActivities(self,deb,fin):
        jsonRequest = '{"query":{"range": {"start_date": {"gte":"'+deb+'","lte":"'+fin+'"}}},"size":10000,"_source":["_id"],"sort":["start_date"]}';
        response = requests.post(self.elasticUrl+"activity/_search",data=jsonRequest,headers={'Content-Type': 'application/json'})
        jsonData = json.loads(response.text)
        return jsonData['hits']['hits']
    
    def getActivitiescX(self):
        #activités non classées
        jsonRequest = '{"query":{"term": {"category.keyword": "cX"}},"size":10000,"_source":["_id"],"sort":["start_date"]}';
        response = requests.post(self.elasticUrl+"activity/_search",data=jsonRequest,headers={'Content-Type': 'application/json'})
        jsonData = json.loads(response.text)
        return jsonData['hits']['hits']
    
    def getActivitiesByCategory(self,category):
        #activités non classées
        jsonRequest = '{"query":{"term": {"category.keyword": "'+category+'"}},"size":10000,"sort":["start_date"]}';
        response = requests.post(self.elasticUrl+"activity/_search",data=jsonRequest,headers={'Content-Type': 'application/json'})
        jsonData = json.loads(response.text)
        return jsonData['hits']['hits']
    
    def getActivitiesNotcX(self):
        #activités non classées
        jsonRequest = '{"query":{"bool":{"must_not":[ {"term": {"category.keyword": "cX"}}]}},"size":10000,"_source":["_id"],"sort":["start_date"]}';
        response = requests.post(self.elasticUrl+"activity/_search",data=jsonRequest,headers={'Content-Type': 'application/json'})
        jsonData = json.loads(response.text)
        return jsonData['hits']['hits']
    
    def updateActivity(self,activity: Activity):
        jsonLaps = "";
        i=0;
        for l in activity.laps:
            if i>0:
                jsonLaps=jsonLaps+","
            jsonLaps=jsonLaps+l.toJSON()
            i=i+1
        jsonData='{"doc":{"label":"'+activity.name+'","cote":"'+str(activity.cote)+'","piste":"'+str(activity.piste)+'","course":"'+str(activity.course)+'","category":"'+activity.category+'","calculatedLaps":['+jsonLaps+']}}'
        
        responseElastic=requests.post(self.elasticUrl+"activity/_update/"+str(activity.id), data=jsonData, headers={'Content-Type': 'application/json'})
        #print(responseElastic)
    
    def updateActivityCategorie(self,activity: Activity):
        
        jsonData='{"doc":{"category":"'+activity.category+'"}}'
        
        responseElastic=requests.post(self.elasticUrl+"activity/_update/"+str(activity.id), data=jsonData, headers={'Content-Type': 'application/json'})
        #print(responseElastic)
    
    def updateActivityLabel(self,activity: Activity):
        
        jsonData='{"doc":{"label":"'+activity.date[0:9]+'_'+activity.name+'"}}'
        
        responseElastic=requests.post(self.elasticUrl+"activity/_update/"+str(activity.id), data=jsonData, headers={'Content-Type': 'application/json'})
        #print(responseElastic)
        
    def updateActivityIndicateurs(self,activity: Activity):
        
        jsonData='{"doc":{"indicateur.speedLaps":'+str(activity.speedLaps)+',"indicateur.recoveryTime":'+str(activity.recoveryTime)+',"indicateur.distanceLaps":'+str(activity.distanceLaps)+',"indicateur.speedLapsWeight":'+str(activity.speedWeighted)+'}}'
        
        responseElastic=requests.post(self.elasticUrl+"activity/_update/"+str(activity.id), data=jsonData, headers={'Content-Type': 'application/json'})
        print(responseElastic)
        
    def getAllCategories(self):
        jsonRequest = '{   "query": {    "match_all": {}  },  "size":"0",  "aggs": {    "category": {      "terms": {        "field": "category.keyword",        "size": 50      }    }  }  }';    
        response = requests.post(self.elasticUrl+"activity/_search",data=jsonRequest,headers={'Content-Type': 'application/json'})
        jsonData = json.loads(response.text)
        buckets = jsonData['aggregations']['category']['buckets']
        ret = []
        for b in buckets:
            ret.append(b['key'])
        return ret
        
    def createCluster(self,id,activities):
        jsonData='{"label":"'+str(id)+'","piste":"false","activities":['
        index=0;
        for activity in activities:
            if index>0:
                jsonData=jsonData+','
            jsonData=jsonData+str(activity.id)
            index=index+1
        jsonData=jsonData+']}'
        print("post cluster :"+id)
        responseElastic=requests.put(self.elasticUrl+"activity_category/_doc/"+str(id), data=jsonData, headers={'Content-Type': 'application/json'})
        #print(responseElastic)

#

